/* eslint-disable */
(function (root, factory) {
    if (typeof define === "function" && define.amd) {
      // AMD
      define([], factory);
    } else if (typeof exports === "object") {
      // Node.js
      module.exports = factory();
    } else {
      // Browser
      var messages = factory();
      for (var key in messages) {
        root.Blockly.Msg[key] = messages[key];
      }
    }
  })(this, function () {
    // This file was automatically generated.  Do not modify.
  
    "use strict";
  
    var Blockly = Blockly || { Msg: Object.create(null) };
  
    // Variables
    Blockly.Msg["NEW_VARIABLE"] = "Créer une variable...";
    Blockly.Msg["VARIABLES_SET"] = "stocker %2 dans la variable %1";
    Blockly.Msg["MATH_CHANGE_TITLE"] = "incrémenter la variable %1 de %2";

    // Functions
    Blockly.Msg["function_def"] = "definir la fonction %1 avec les arguments %2";
    Blockly.Msg["function_call"] = "appeler la fonction %1 avec les arguments %2";
    Blockly.Msg["function_return"] = "renvoyer %1";

    // Graph
    Blockly.Msg["matplotlib_plot"] = 'tracer %2 en fonction de %1 avec le nom %3, la couleur %4 et le marqueur %5';
    Blockly.Msg["matplotlib_plot2"] = 'tracer %2 en fonction de %1 avec les options %4 et le nom %3';
    Blockly.Msg["matplotlib_show"] = 'afficher le graphique';
    Blockly.Msg["matplotlib_draw"] = 'dessiner le graphique';
    Blockly.Msg["matplotlib_title"] = 'définir le titre du graphique à %1';
    Blockly.Msg["matplotlib_xlabel"] = 'définir le titre de l\'axe des abscisses à %1';
    Blockly.Msg["matplotlib_ylabel"] = 'définir le titre de l\'axe des ordonnées à %1';
    Blockly.Msg["matplotlib_legend"] = 'afficher la légende';
    Blockly.Msg["matplotlib_grid"] = 'afficher la grille';
    Blockly.Msg["matplotlib_clf"] = 'effacer tous les graphiques';
    
    // Import
    Blockly.Msg["import_numpy"] = 'importer numpy sous le nom np';
    Blockly.Msg["import_turtle"] = 'importer toutes les fonctions de turtle';
    Blockly.Msg["import_matplotlib"] = 'importer matplotlib.pyplot sous le nom plt';
    Blockly.Msg["import_random"] = 'importer random';
    Blockly.Msg["import_time"] = 'importer time';
    Blockly.Msg["import_math"] = 'importer math';
    Blockly.Msg["import_pil"] = 'importer PIL.Image sous le nom Image';
    Blockly.Msg["import_networkx"] = 'importer networkx sous le nom nx';

    // Lists
    Blockly.Msg["list_create"] = 'stocker %2 dans la variable %1';
    Blockly.Msg["list_add"] = 'ajouter %2 à %1';
    Blockly.Msg["list_get"] = '%2 ème élément de %1';
    Blockly.Msg["list_set"] = 'modifier le %2 ème élément de %1 par %3';

    // Conditions
    Blockly.Msg["condition_if"] = "si %1 alors";
    Blockly.Msg["condition_else"] = "sinon";
    Blockly.Msg["condition_elif"] = "sinon si %1";
    Blockly.Msg["condition_and"] = "et";
    Blockly.Msg["condition_or"] = "ou";
    Blockly.Msg["condition_not"] = "n'est pas %1";
  
    // Loops
    Blockly.Msg['boucle_for'] = "pour %1 allant %2";
    Blockly.Msg['boucle_for_in'] = "pour %1 dans %2";
    Blockly.Msg['boucle_while'] = "tant que %1";

    // Math
    Blockly.Msg["math_cos"] = "le cosinus de la valeur %1";
    Blockly.Msg["math_sin"] = "le sinus de la valeur %1";
    Blockly.Msg["math_tan"] = "la tangente de la valeur %1";
    Blockly.Msg["math_acos"] = "l'arc cosinus de la valeur %1";
    Blockly.Msg["math_asin"] = "l'arc sinus de la valeur %1";
    Blockly.Msg["math_atan"] =  "l'arc tangente de la valeur %1";
    Blockly.Msg["math_exp"] =   "l'exponentielle de la valeur %1";
    Blockly.Msg["math_log"] =  "le logarithme népérien de la valeur %1";
    Blockly.Msg["math_log10"] = "le logarithme décimal de la valeur %1";
    Blockly.Msg["math_sqrt"] = "la racine carrée de la valeur %1";
    Blockly.Msg["math_pow"] = "la valeur %1 à la puissance %2";
    Blockly.Msg["math_ceil"] = "l'arrondi supérieur de la valeur %1"
    Blockly.Msg["math_floor"] = "l'arrondi le plus proche de la valeur %1"
    Blockly.Msg["math_abs"] = "valeur absolue de la valeur %1"
    Blockly.Msg["math_round"] =   "arrondi de la valeur %1";
    Blockly.Msg["math_trunc"] =  "troncature de la valeur %1";
    Blockly.Msg["math_min"] = "valeur minimale de %1";
    Blockly.Msg["math_max"] = "valeur maximale de %1";
    Blockly.Msg["math_add"] = "l'addition";
    Blockly.Msg["math_sub"] = "la soustaction";
    Blockly.Msg["math_mul"] = "la multiplication";
    Blockly.Msg["math_div"] = "la division";
    Blockly.Msg["math_mod"] = "le reste de la division euclidienne";
    Blockly.Msg["math_pow2"] = "puissance";
    Blockly.Msg["math_floordiv"] = "le quotient de la division euclidienne";
    Blockly.Msg["math_operator"] = "%2 de %1 par %3";

    Blockly.Msg["numpy_array"] = "le tableau de %1";
    Blockly.Msg["numpy_polyfit"] = "le modèle de régression polynomiale de degré %3 de %2 en fonction de %1";


    // PIL
    Blockly.Msg["pil_open"] = 'ouvrir l\'image située à l\'adresse %2 et la stocker dans %1';
    Blockly.Msg["pil_show"] = 'afficher l\'image %1';
    Blockly.Msg["pil_size"] = 'la taille de l\'image %1';
    Blockly.Msg["pil_resize"] = 'redimensionner l\'image %2 à la taille %3 et la stocker dans %1';
    Blockly.Msg["pil_rotate"] = 'faire tourner l\'image %2 de %3 degrés et la stocker dans %1';
    Blockly.Msg["pil_crop"] = 'recadrer l\'image %2 en fonction des coordonnées %3 et la stocker dans %1';
    Blockly.Msg["pil_getpixel"] = 'la couleur du pixel de l\'image %1 aux coordonnées %2';
    Blockly.Msg["pil_putpixel"] = 'modifier la couleur du pixel de l\'image %1 aux coordonnées %2 par la couleur %3';
    
    // Random
    Blockly.Msg["random_random"] = "nombre aléatoire entre 0 et 1";
    Blockly.Msg["random_randint"] = "entier aléatoire entre %1 et %2";
    Blockly.Msg["random_choice"] = "nombre aléatoire dans la liste %1";
    Blockly.Msg["random_shuffle"] = "changer l'ordre des éléments de la liste %1";
    Blockly.Msg["random_uniform"] = "nombre aléatoire entre %1 et %2";
    Blockly.Msg["random_randrange"] = "entier aléatoire entre %1 et %2 avec un pas de %3";
    Blockly.Msg["random_sample"] = "%2 éléments aléatoires dans la liste %1";
    Blockly.Msg["random_seed"] = "initialiser le générateur de nombres aléatoires avec la graine %1";

    // Statements
    Blockly.Msg["time_sleep"] = "attendre %1 secondes";
    Blockly.Msg["print"] = "afficher %1";
    Blockly.Msg["input"] = "afficher le message %2 pour entrer une valeur et la stocker dans %1";
    Blockly.Msg["pass"] = "passer";
    Blockly.Msg["len"] = "la longueur de %1";
    Blockly.Msg["type"] = "afficher le type de %1";
    Blockly.Msg["int"] = "la conversion de %1 en entier";
    Blockly.Msg["float"] = "la conversion de %1 en flottant";
    Blockly.Msg["str"] = "la conversion de %1 en chaîne de caractères";
    Blockly.Msg["list"] = "la conversion de %1 en liste";
    Blockly.Msg["round"] = "l'arrondi de %1";
    Blockly.Msg["range"] = "de %1 à %2 avec un pas de %3";
    Blockly.Msg["list_def"] = "la liste de valeurs %1";
    Blockly.Msg["tuple_def"] = "( %1 )";
    Blockly.Msg["string"] = "la chaîne de caractères %1";
    Blockly.Msg["boolean"] = "%1";

    // Turtle
    Blockly.Msg["turtle_init"] = "initialiser la tortue %1";
    Blockly.Msg["turtle_shape"] = "changer la forme de la tortue %1 en %2";
    Blockly.Msg["turtle_mainloop"] = "lancer la boucle principale de la tortue";
    Blockly.Msg["turtle_forward"] = "faire avancer la tortue %1 de %2";
    Blockly.Msg["turtle_backward"] = "faire reculer la tortue %1 de %2";
    Blockly.Msg["turtle_right"] = "faire tourner la tortue %1 de %2 degrés vers la droite";
    Blockly.Msg["turtle_left"] = "faire tourner la tortue %1 de %2 degrés vers la gauche";
    Blockly.Msg["turtle_penup"] = "relever le stylo de la tortue %1";
    Blockly.Msg["turtle_pendown"] = "abaisser le stylo de la tortue %1";
    Blockly.Msg["turtle_pencolor"] = "changer la couleur du stylo de la tortue %1 en %2";
    Blockly.Msg["turtle_speed"] = "changer la vitesse de la tortue %1 en %2";
    Blockly.Msg["turtle_width"] = "changer la largeur du stylo de la tortue %1 en %2";
    Blockly.Msg["turtle_goto"] = "déplacer la tortue %1 aux coordonnées x: %2 y: %3";
    Blockly.Msg["turtle_circle"] = "dessiner un cercle de rayon %2 avec la tortue %1 avec un angle de %3 degrés";

    // Networkx
    Blockly.Msg["networkx_graph"] = "Créer un graphe vide %1";
    Blockly.Msg["networkx_add_node"] = "Ajouter le noeud %2 au graphe %1";
    Blockly.Msg["networkx_add_edge"] = "Ajouter l'arête entre %2 et %3 au graphe %1";
    Blockly.Msg["networkx_draw"] = "Dessiner le graphe %1 avec les labels %2";
    Blockly.Msg["networkx_number_of_nodes"] = "Nombre de noeuds du graphe %1";
    Blockly.Msg["networkx_number_of_edges"] = "Nombre d'arêtes du graphe %1";
    Blockly.Msg["networkx_diameter"] = "Diamètre du graphe %1";
    Blockly.Msg["networkx_radius"] = "Rayon du graphe %1";
    Blockly.Msg["networkx_center"] = "Centre du graphe %1";

    // Comments
    Blockly.Msg["comment"] = "Commentaire : %1";

    return Blockly.Msg;
  });
  